<?php include('koneksi.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vigz View</title>.

    <style>

        table {
            border: 1px solid;
            border-collapse: collapse;
            width : 70%;
            margin : 10px auto 10px auto;
        }
        table thead th {
            background-color : $ddefef;
            border : 1px solid;
            padding: 10px;
            text-align: left;
        }
        table tbody td{
            border: 1px solid;
            text-align: center;
        }

        img {
            width : 120px;
        }
    </style>
</head>
<body>
    <center><h1>Data Film</h1></center>
    <center><a href="tambah_film.php">+ &nbsp;Tambah Film</a></center>
    <table>
        <thead>
            <tr>
                <th>No.</th>
                <th>Gambar</th>
                <th>Judul</th>
                <th>Deskripsi</th>
                <th>Edit Film</th>
                <th>Hapus Film</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $query = "SELECT * FROM add_film ORDER BY id_film ASC";  
            $result = mysqli_query($koneksi, $query);

            if(!$result) {
                die("Query Erorr : ".mysqli_errno($koneksi)." - ".mysli_error($koneksi));
            }
            $no = 1;

            while ($row = mysqli_fetch_assoc($result)) :
                
            ?>
            
            <tr>
                <td><?php echo $no; ?></td>
                <td><img src="gambar/<?php echo $row['gambar']?>" alt=""></td>
                <td><?php echo $row['judul']; ?></td>
                <td><?php echo $row['deskripsi']; ?></td>
                
                <td>
                    <a href="edit_film.php?id_film=<?php echo $row['id_film']; ?>">Edit Film</a>
                </td>
                
                <td>
                    <a href="proses_hapus.php?id_film=<?php echo $row['id_film']; ?> " onclick ="return confirm ('Anda yakin ingin menghapus film ini ?')"> Hapus</a>
                </td>
            </tr>

            <?php 
            $no++;
        endwhile
        ?>
        </tbody>
    </table>

</body>
</html>