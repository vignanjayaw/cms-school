<?php include('koneksi.php');


if(isset($_GET['id_film'])) {

$id_film = $_GET['id_film'];
$query = "SELECT * FROM add_film where id_film = '$id_film'";
$result = mysqli_query($koneksi, $query);
$data = mysqli_fetch_assoc($result);

if(!count($data)) {
    echo "<script>alert('Data tidak ditemukan pada tabel');window.location='index.php';</script>";
}

}else{
    echo "<script>alert('Masukkan ID yang ingin di edit');window.location='index.php';</script>";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vigz View</title>
    <style type="text/css">
        label {
            margin-top: 10px;
            float: left;
            text-align: left;
            width: 100%;
        }
        input {
            padding: 6px;
            width:100%;
            box-sizing: border-box;
            border: 2px solid;
            outline-color: salmon;
        }
        .base{
            width:400px;
            padding:20px;
            margin-left:auto;
            margin-right:auto;
        }
        button {
            background-color: salmon;
            color:#fff;
            padding: 10px;
            font-size: 12px;
            border: 0;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <center><h1>Edit Film <?php echo $data['judul']  ?></h1></center>
    <form action="proses_edit.php?id_film=<?=$_GET['id_film']?>" method="POST" enctype="multipart/form-data">
    <section class="base">
    <div>
            <label for="">Cover Film</label>
            <img src="gambar/<?php echo $data['gambar'];?>" style="width: 120px; float:left; margin-bottom:5px;"  >
            <input type="file" name="gambar"   />
            <i style="float:left;font-size:11px;color:red;">Abaikan jika tidak merubah cover film</i>
        </div>
        <div>
            <label for="">Nama Film</label>
            <input type="text" name="judul" autofocus="" required="" value ="<?php echo $data['judul'];  ?>" />
        </div>
        <div>
            <label for="">Deskripsi</label>
            <input type="text" name="deskripsi" required="" value ="<?php echo $data['deskripsi'];  ?>">
        </div>
        <div>
            <br>
            <button type="submit">Simpan Perubahan</button>
        </div>
    </section>
    </form>
</body>
</html>