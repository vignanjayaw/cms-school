<?php include('koneksi.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vigz View</title>
    <style type="text/css">
        label {
            margin-top: 10px;
            float: left;
            text-align: left;
            width: 100%;
        }
        input {
            padding: 6px;
            width:100%;
            box-sizing: border-box;
            border: 2px solid;
            outline-color: salmon;
        }
        .base{
            width:400px;
            padding:20px;
            margin-left:auto;
            margin-right:auto;
        }
        button {
            background-color: salmon;
            color#fff;
            padding: 10px;
            font-size: 12px;
            border: 0;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <center><h1>Tambah Film</h1></center>
    <form action="proses_tambah.php" method="POST" enctype="multipart/form-data">
    <section class="base">
    <div>
            <label for="">Cover Film</label>
            <input type="file" name="gambar"  required="" />
        </div>
        <div>
            <label for="">Nama Film</label>
            <input type="text" name="judul" autofocus="" required="" />
        </div>
        <div>
            <label for="">Deskripsi</label>
            <input type="text" name="deskripsi" required="">
        </div>
        <div>
            <button type="submit">Simpan Film</button>
        </div>
    </section>
    </form>
</body>
</html>